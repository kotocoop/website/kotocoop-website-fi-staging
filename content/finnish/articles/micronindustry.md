---
title: Mikroteollisuus paikallisten kestävien talouksien selkärankana
hide: false
aliases:
  - /articles/micro-industry-as-a-backbone-of-local-sustainable-economies/
  - /articles/micro-industry
date: 2021-05-02 11:30:00 +0000
---
> Mikroindustriateknologian kehittämisen avulla voimme mahdollistaa yhteisöllisten talouksien taloudellisen, ekologisen ja sosiaalisen kestävyyden.

Pienet yhteisöpohjaiset taloudet ovat monien valitsema polku vetäytyä vakiintuneesta globaalista taloudesta. Merkittävin esimerkki tällaisesta taloudesta ovat ekokylät, jotka ovat olleet olemassa monissa maissa jo jonkin aikaa. Ihmisiä tällaisiin yhteisöllisiin talouksiin työntävät yleensä sosiaaliset ja ekologiset arvot. Ei ole harvinaista, että taloudellisesti puhuen näillä pienillä talousjärjestelmillä on vaikeuksia saavuttaa kestävyyttä. Taloudellinen kestävyys tässä tarkoittaa elinkeinon kestävyyttä. Ekokylien tapauksessa tämä voisi tarkoittaa, että ekokylä kärsii rahatulojen puutteesta yhteisön tukemiseksi tai että ekokylä pyrkii täydelliseen omavaraisuuteen huonolla menestyksellä, mikä johtaa asukkaidensa elämänlaadun heikkenemiseen. Taloudellisen kestävyyden puute voi heikentää myös ekologista ja sosiaalista kestävyyttä. Miten voisimme yrittää korjata tämän taloudellisen ongelman samalla kun viemme yhteisöllisen taloudellisen toiminnan aivan uudelle tasolle prosessissa?



Avaintekijä taloudellisessa kestävyydessä nykytaloudessa on teollinen toiminta. Teollinen vallankumous on tuonut suurta vaurautta ihmiskunnalle ja nykyään teollisuus on kaikkien kehittyneiden talouksien ydin. Teollinen toiminta tuo tuotantoon sellaista tehokkuutta, joka vaikuttaa mahdottomalta saavuttaa muilla keinoilla.



Skaalautuvuus on perinteisesti ollut suuri tekijä teollisuuden tehokkuuden taustalla. Suurten volyymien tuottaminen helpottaa yksikkökustannusten alentamista, mikä tekee tuotannosta tehokkaampaa ja kannattavampaa. Tätä ilmiötä kutsutaan skaalaetuuksiksi. Kuitenkin suuren mittakaavan teollinen toiminta vaatii raskasta infrastruktuuria ja suuria investointeja, mikä tekee siitä melko jäykän ja epäkäytännöllisen pientalouksille, kuten ekokyliin. Lisäksi tällaisen suuren laitoksen toiminnan ylläpitäminen olisi lähes mahdotonta pienelle yhteisölle, jos se haluaa olla sosiaalisesti kestävä. Lyhyesti sanottuna perinteinen teollinen toiminta ei sovi perinteisiin yhteisöllisiin talouksiin.



"Mikroteollinen" toiminta tarjoaa tavan yhdistää yhteisölliset taloudet teollisuuden tehokkuuteen. Mikroteollisuus on pienimuotoista teollista toimintaa, jota olisi mahdollista harjoittaa yhteisöllisellä lähestymistavalla. Mikroteollisuus pyrkisi saavuttamaan teollisen tehokkuuden ilman tarvetta suurille volyymeille. Kuitenkin jopa pienimuotoisesta teollisesta toiminnasta saataisiin paljon enemmän tehokkuutta skaalaetuuksien avulla kuin pelkästään käsityöllä.



Tekijät, jotka voisivat tehdä mikroteollisuuden mahdolliseksi, ovat automaatio, uudelleenkonfiguroitavuus ja avoin tieto. Automaatio mahdollistaa korkean laadun saavuttamisen vähemmällä ihmistyöllä. Tämä on ratkaisevan tärkeä näkökohta pyrittäessä toteuttamaan teollista toimintaa, jota voitaisiin soveltaa ja harjoittaa pienessä ja yhteisöllisessä talousjärjestelmässä. Uudelleenkonfiguroitava valmistus korostaa modulaarisuutta ja joustavuutta, mikä mahdollistaa samojen koneiden käytön monipuolisemmin. Tätä voitaisiin käyttää tuottamaan erilaisia laadukkaita tuotteita pienemmissä tiloissa.



Tärkeä näkökohta tässä kehityksessä on avoimen tiedon jakaminen. Kaikenlaisten piirustusten, koneiden kokoamisen ja ohjelmistojen sekä tietysti kaikkien liittyvän teknologian modulaaristen parannusten tiedot tulisi jakaa avoimesti ja globaalisti. Tämä mahdollistaisi kehityksen ilman rajoituksia. Kun kaikki tieto jaetaan avoimesti, ihmiset ympäri maailmaa voivat hyötyä edistyksestä.



Näiden ja muiden mahdollistajien avulla voisimme alkaa kehittää valmistusteknologiaa, joka tekisi mikroteollisen toiminnan mahdolliseksi. Termi, joka kuvaa teknologista kehitystä hyvin, on "efemeralisaatio", ja se tarkoittaa enemmän saavuttamista vähemmällä. Efemeralisaatio on ilmiö, joka varmistaa, että mikroteollinen toiminta on mahdollista, jos päädymme siihen suuntaan. Perinteisesti efemeralisaatiota on hyödynnetty tuottamaan vielä enemmän tavaroita valtavissa laitoksissa äärimmäisen tehokkaasti ja hyvällä voitolla. Nämä laitokset eivät kuitenkaan harvoin ole sosiaalisesti tai ekologisesti kestäviä. Mutta efemeralisaatiota voitaisiin myös hyödyntää kehittämään mikroteollisia laitoksia, joissa tuotetaan tarpeeksi mahdollisimman vähällä. Kun tämä kehityspolku omaksutaan, meidän tulisi nähdä paikallisesti toimivia mikroteollisia laitoksia, joilla on jatkuvasti kasvava tehokkuus, laatu ja monipuolisuus.



Esimerkkinä tarkastellaan teräksen materiaalivirtaa. Terästä käytetään laajalti teollisuudessa ja infrastruktuurissa. Siksi ekologinen ja kiertotalouden mukainen tapa hallita teräksen virtaa taloudessa on ratkaisevan tärkeää talouden kestävyydelle. Mikroteolliset teräslaitokset sisältäisivät koneita teräksen kierrätykseen, kierrätetyn teräksen jalostukseen ja teräksen valmistukseen. Aluksi ehkä kehitettäisiin vain teräksen valmistuslaitteita, kuten CNC-, hitsaus- ja taivutuskoneita. Näitä voitaisiin käyttää muiden mikroteollisten tarpeiden terästuotteiden valmistukseen. Avoimen lähdekoodin kehitystyön avulla parempia ja edistyneempiä koneita voidaan kehittää nopeasti. Jos suunnitelmat ovat modulaarisia, edistystä voidaan saavuttaa vielä tehokkaammin. Edistyneemmät koneet tuottaisivat korkeampaa laatua suuremmalla automaatiolla. Kun koneet paranevat, niitä käytetään enemmän, mikä johtaisi nopeampaan kehitykseen. Tämä lumipalloefekti voi levitä kaikkiin teräksen käsittelyyn tarvittaviin koneisiin ja kaikille muillekin aloille. Mitä paremmat koneet ovat, sitä enemmän talous voi hyötyä niistä. Efemeralisaation voiman avulla etenevä avoimen lähdekoodin teknologia, joka leviää kaikille teollisuuden aloille, voisi mahdollistaa kasvavan vaurauden ja kestävyyden.



Mikroteolliset teknologiat voisivat vaikuttaa suuresti pienempien talouksien taloudelliseen kestävyyteen tarjoten joukon mahdollisuuksia järjestää yhteiskuntaa ekologisesti ja sosiaalisesti. Asianmukaisesti suunnitellut mikroteolliset laitokset voisivat tarjota suurimman osan pienelle taloudelle tarvittavista hyödykkeistä paikallisesti ja yhteisöllisesti toimien. Mikroteollisuus voi myös tehdä sosiaalisen ja ekologisen kestävyyden toteutettavaksi suuremmille yhteisötalouksille kuin pelkät ekokylät. Globaalin talouden vähemmän keskitetty tekeminen voi olla ratkaisevaa sosiaalisen, ekologisen ja taloudellisen kestävyyden saavuttamiseksi maailmanlaajuisesti.
