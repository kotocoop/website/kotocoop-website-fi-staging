---
title: Veganismi ja Koto
date: 2021-07-15 15:30:00 +0000
tags:
  - earth_restoring_lifestyle
images:
  - /img/blog/sheep.jpg
---
Me Koto osuuskunnassa tiedämme, että vegaaninen elämäntapa on eettisempi muita olentoja kohtaan ja tilastojen mukaan ekologisempi.

Veganismissa on pohjimmiltaan kyse tarpeettoman kärsimyksen vähentämisestä. Vegaanit laajentavat kärsimyksen käsitteen koskemaan kaikkia eläimiä, mukaan lukien ihmiset. He hyväksyvät, että myötätunnon lisääminen ja aiheuttamamme kärsimyksen lieventäminen on olennaisempaa kuin makuhermojemme tai tottumustemme tyydyttäminen tai vähäisen vaivan välttäminen.

Näistä syistä Koto kannattaa vegaanista elämäntapaa. Koto-yhteisöön kuuluvilta ei vaadita veganismia,  mutta pyrimme kertomaan ihmisille tosiasioita eri ruokavalioiden ja elämäntapojen vaikutuksista.

Veganismi on muutakin kuin se, minkä mukaisesti monet vegaanit elävät. Koto tunnustaa, että monet, jotka eivät noudata vegaaniruokavaliota, tekevät paljon vähentääkseen kärsimystä ja ekologisia ongelmia maailmassa.

Nykyisin yhteiskunta suosii kaikkiruokaista ruokavaliota. Muuttamalla yhteiskuntamme perusrakennetta toivomme voivamme auttaa kansalaisia tekemään tietoisempia valintoja, jotka vastaavat heidän uskomuksiaan. Koto ei velvoita ketään käyttäytymään tavalla, jota hän ei halua, mutta pyrimme valistamaan ja opastamaan ihmisiä toimimaan tieteellisiin periaatteisiin perustuvalla kestävällä tavalla.

Koto-yhteisö tuottaa todennäköisesti vain kasvisruokaa, koska kasviperäinen ruokavalio on useimmille jäsenillemme tärkeä asia ja yhteisön tulisi heijastaa jäsenten arvoja. Erilaisia maailmankatsomuksia tulisi kuitenkin kunnioittaa, vaikka niissä on eroavuuksia. Mielipiteiden muuttaminen vaatii vaivaa ja aikaa, ja meidän on ymmärrettävä se.

Koton toimintayksiköt voivat olla täysin ”vegaanisia”; se riippuu yksikön säännöistä. Jotkut ihmiset haluaisivat pitää lemmikkejä tai esimerkiksi pelastaa kotieläimiä asumaan yksikössä. Se, mitä yksikön säännöt sallivat, on sen perustaneiden ihmisten päätettävissä.

Koton nopea laajentumissuunnitelma luo todennäköisesti tilanteita, joissa kaikki Koton asukkaat eivät ole kasvissyöjiä. Koto pyrkii silti levittämään tietoa ja toimimaan esimerkkinä auttaen ihmisiä sekä vegaanisen elämäntavan, että muiden yhteiskunnallisten ja  teknologisten näkökohtien osalta.
