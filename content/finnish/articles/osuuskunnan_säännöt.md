---
title: Elokoto osuuskunnan säännöt
date: 2024-04-02 10:00:00 +0000
---
# Koto osuuskunnan säännöt

## 1 § Toiminimi ja kotipaikka
Osuuskunnan nimi on Koto osuuskunta ja kotipaikka Hämeenlinna.

## 2 § Osuuskunnan toiminnan tarkoitus ja toimiala
Toiminnan tarkoituksena on tukea ja helpottaa jäseniensä toimeentuloa tarjoamalla heille tuotteita ja palveluita.
Tarkoituksensa toteuttamiseksi osuuskunta tarjoaa seuraavia palveluita: Tori- ja markkinakauppa, insinööripalvelut ja niihin liittyvä tekninen konsultointi, kasvinviljely ja siihen liittyvät palvelut, asuin- ja muiden rakennusten rakentaminen, sähkön tuotanto vesi- ja tuulivoimalla.
Osuuskunta voi myydä palveluita sekä tuotteita myös osuuskunnan ulkopuolelle.

## 3 § Ylijäämän jakaminen
Kaikki ylijäämä käytetään osuuskunnan laajenemiseen.

## 4 § Jäsenyys
Osuuskunnan jäseneksi pääsee maksamalla osuusmaksun. Kaikilla jäsenillä on äänioikeus osuuskuntakokouksessa.

## 5 § Osuusmaksu
Osuuskunnan hallitus määrittelee osuusmaksun vuosittain.

## 6 § Hallitus
Osuuskunnalla on hallitus, johon kuuluu yhdestä viiteen varsinaista jäsentä ja ainakin yksi varajäsen, mikäli hallitukseen valitaan vähemmän kuin kolme varsinaista jäsentä.
Hallituksen jäsenten toimikausi jatkuu toistaiseksi.
Uusi hallitus valitaan siten, että virkaatekevä hallitus tekee esityksen uuden hallituksen kokoonpanosta. Kokoonpano hyväksytään osuuskunnan kokouksessa.

## 7 § Osuuskunnan edustaminen
Osuuskuntaa edustaa hallitus. Hallitus voi lisäksi antaa nimetylle henkilölle prokuran tai oikeuden osuuskunnan edustamiseen

## 8 § Osuuskunnan kokous
Jäsenet käyttävät päätösvaltaansa sille lain tai sääntöjen mukaan kuuluvissa asioissa osuuskunnan kokouksissa.
Kokous pidetään osuuskunnan kotipaikassa. Kokous voidaan pitää myös muulla paikkakunnalla, jossa osuuskunnalla on toimipaikka.
Osuuskunnan kokouksen koolle kutsumisessa, kokouskutsun sisällössä, kokouskutsuajassa ja kutsutavassa sekä kokousasiakirjojen nähtävillä pitämisessä ja lähettämisessä noudatetaan osuuskuntalain 5 luvun määräyksiä. Kokouskutsu lähetetään sähköpostilla jäsenen ilmoittamaan sähköpostiosoitteeseen tai muulla sähköisellä tavalla.

## 9 § Varsinainen osuuskunnan kokous
Varsinainen osuuskunnan kokous on pidettävä kuuden kuukauden kuluessa tilikauden päättymisestä
Kokouksessa on päätettävä:
- tilinpäätöksen vahvistamisesta
- taseen osoittaman ylijäämän käyttämisestä
- vastuuvapauden myöntämisestä hallituksen jäsenille
- osuuskunnan toiminnantarkastaja

## 10 § Tilikausi ja tilinpäätös
Osuuskunnan tilikausi on kalenterivuosi. Kultakin tilikaudelta on laadittava tilinpäätös.
Tilinpäätös on annettava tilintarkastajalle vähintään kuukautta ennen sitä kokousta, jossa tuloslaskelma ja tase esitetään vahvistettaviksi.

## 11 § Tilintarkastaja ja toiminnantarkastaja
Osuuskunnalle valitaan yksi tilintarkastaja ja varatilintarkastaja, jos tilintarkastuslaki sitä edellyttää. Tilintarkastajan toimikausi on toistaiseksi voimassa oleva.
Osuuskunnalla ei ole velvollisuutta valita tilintarkastajaa eikä toiminnantarkastajaa ellei osuuskunnan kokous toisin päätä.

## 12 § Yksiköt
Yksikkö on nimetty ryhmä osuuskunnan jäseniä jotka noudattavat osuuskunnan asettamia yksikön sääntöjä.
Yksikkö valitsee keskuudestaan vastuuhenkilön joka toimii yhteyshenkilönä ja on velvollinen valvomaan yksikön sääntöjen noudattamista sekä raportoimaan osuuskunnan hallitukselle toiminnasta yksikön sääntöjen mukaisesti.
Kaikki osuuskunnan jäsenet kuuluvat yhteen yksikköön. Jäsenet voivat pyytää siirtoa toiseen yksikköön ja sekä osuuskunnan hallituksen, että yksikön yhteyshenkilön tulee hyväksyä siirto.
Osuuskunta voi halutessaan purkaa yksikön jolloin yksikköön kuuluvat jäsenet erotetaan.

## 13 § Sääntöjen muuttaminen
Sääntöjen muuttamisesta päättää osuuskunnan kokous. Hallitus hyväksyy sääntömuutokset.

## 14 § Yleismääräys
Muilta osin noudatetaan voimassa olevaa osuuskuntalakia.

