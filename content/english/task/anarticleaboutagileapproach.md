---
title: An article about agile approach
date: 2023-01-27 10:00:00
layout: singletask
aliases:
- /task/anarticleaboutagileapproach
---

## Goal of this task

Lets write an article that explains why Koto is taking the approach that we shouldn't try design a too big system and make plans to implement it in the future, but use a more iterative evolution of ideas and implementations.

## About the task

Ask _Juuso V and juuso@vilmunen.net here_ for editing rights.

Don’t use too much time on one topic in the first go. Go through all the parts and start over if you have more time. So iterative here too.

# Planning

## Issues

Issues I want to address:

- Lifestyle waterfall. Designing a smart village and hoping hundreds of people join
- Software before users. for example 'decision making', timebank,  'design systems'
- Trying to educate millions if people before acting

## Why we cannot know the solution

Yeah, why?

## Steps

What steps should be described in the artcle?

# Article Content

The Agile methodology emerged in the 1990s as a response to the "waterfall" approach to software development, which was characterized by a linear, sequential approach to project management. "Waterfall" involved extensive planning, signing off on the plan and sticking to it for the next long period of time. The Agile approach, by contrast, emphasized flexibility, collaboration, and iterative development. The development cycles are small and the whole project is subject to change at any time. The Agile Manifesto, a set of principles for Agile software development, was written in 2001. It is an attempt at having people change the way they're thinking about work in order to increase the productivity. The focus should be the end product.

Scrum, which is a specific implementation of the Agile methodology, was quickly introduced in the same decade. Scrum was formalized as a framework for project management that emphasized self-organizing teams and frequent inspection and adaptation. It is based around a concept of the Scrum ceremonies, the main being "sprint" which is a devcelopment cycle that is usually two to four weeks long.

A series of tasks are made that constitute a body of work the team will be assigned to. A number of tasks is chosen for a sprint which the team commints to finish within the sprint time. The tasks are chosen on the sprint planning meeting and each member of the team should get to know what the task is about even though not all of them are going to work on it. This facilitates the group decision making and horizontal knowledge sharing about the subject of the task.

Once the team commits and the sprint starts, each day of the sprint starts with a short daily 15 minute stand-up meeting. This is for each member to answer what they worked on the day before, what their plan is for the current day and if they have any problems. The daily stand-up meetings eliminate the need for long meetings and interruptions during the day.

The sprint culminates in the demonstration of the work done and eventually in the retrospective meeting which is in reality the post-mortem of the sprint. The team mebers try to figure out if something went wrong with the sprint and how to fix it.

Moving forward, the cycle repeats. With each cycle the team keeps adjusting. They learn how to work within the methodology. The tasks are also made smaller to fit into the cycle or if research is needed, a research task is made and it fits within the sprint. With each sprint the team starts to exhibit the performance curve known as "velocity" which shows what they can commit to. Usually within the team there's a role that one person takes on to enforce the following of the methodology. The goal of that person is to make themselves obsolete because it is in everyone's interest in the team to be as much self-organizing as possible.

Since the 1990s, Scrum and Agile have gained widespread acceptance and have been adopted by many organizations across a range of industries. They have continued to evolve and develop as practitioners have refined and adapted the methodology to suit their specific needs and contexts.

While it is difficult to quantify the exact level of efficiency that can be gained from using Scrum and Agile, many organizations have reported significant improvements in their development processes, including faster time-to-market, better quality software, and higher customer satisfaction.

It is important to note, however, that Scrum and Agile require a significant amount of discipline and commitment from the team to be successful. They are not a silver bullet solution that will magically make a project run smoothly. But when implemented properly, they can be highly effective in improving the efficiency and effectiveness of software development projects.

Of course, there is criticism with people claiming them to be too restrictive and can devolve into micromanagement. Usually the failure of the people happens opposed to the failure of methodology. One must have the right mindset to work within the Scrum/Agile which is often not the case.

The methodology has been found to be highly effective in a wide range of industries and projects beyond software development. The fundamental principles of Scrum and Agile, such as collaboration, iterative development, and flexibility, can be adapted to suit different types of projects and industries.

Scrum/Agile have been successfully used in marketing, product development, education, project management, construction, and healthcare.

One of the reasons why Scrum and Agile can be applied to different industries and projects is that they are based on a flexible, iterative approach to project management. The focus on collaboration and continuous improvement allows teams to be more responsive to changing requirements and to adapt quickly to new challenges.

However, it is important to note that adapting Scrum and Agile to different industries and projects may require some customization to ensure that the methodology is aligned with the specific needs and constraints of the project. It is also essential to have a deep understanding of the principles and values of Scrum and Agile in order to apply them effectively in non-software development contexts.



