---
layout: post
title: 'A new article about Koto-model'
sub_heading: ''
date: 2020-07-05T22:00:00.0000
tags: []
images:
- /img/blog/default.png
related_posts: []

---

A new article of Koto model of operations has been published [here](/about/model/).

Stay tuned for more articles about different types of units, teams, technology, projects and progress we are making. 
