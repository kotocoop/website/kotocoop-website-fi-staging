---
layout: post
title: 'Summer 2022 in Auroora'
sub_heading: ''
date: 2022-04-27 10:30:00 +0000
tags: [ "auroora" ]

images:
- /img/units/auroora/2021-auroora-brighter-P9122643_front.jpg

related_posts: []

---

We are looking for a group of friends, a family or individuals, to spend a summer in Finnish countryside at our property called Auroora. Here is more information about the place [https://kotocoop.org/units/auroora/](https://kotocoop.org/units/auroora/)

We don't want the place to be empty for the summer so we are offering it for use with a "pay what you can" deal. A great option would be if somebody wanted to start a garden with raised beds and stay until harvest. Our co-op can fund the materials needed for that, residents would then own the products.

No animals are used for production in Auroora, but you can bring pets if you like.

The building is big and we are currently renovating it, and some people will probably move in permanently in the autumn. Current parts of the building that are in good condition can be used as a fully functional apartment or apartments, but for the summer we are not looking for permanent inhabitants (there is a possibility that people could stay there for longer, or even permanently, but we are not sure about that yet).

In the middle of the building there's a shared kitchen and dining room, a shower and a bathroom.

For private space on the other end of the building we offer one 71 m² room divided into living room and bedroom by a permanent room divider and a 33 m² bedroom.

The other areas of the building will be renovated during the summer or before so it is possible that room for more people will become available. During the renovation people might come and go and use the common areas too; members of our organization visiting the place, people doing the renovation, electricians and so on.

The land area is free to use for leisure and gardening. Total area is a hectare, and about half of that is forest.

We also have a car (manual transmission) and an electric bicycle (a "fatbike") that are free for use for temporary residents too. We can also discuss what furniture is needed and purchase those since they will be left in Auroora.

Closest grocery store is located in Rääkkylä, about 16km from Auroora.

Recycling is a must. Recycling bins for cans, paper and glass are by the driveway and for plastic at the grocery store.

If you are interested, please fill out this [form](https://cloud.kotocoop.org/index.php/apps/forms/7BJmKYG5Y4XDqswY)