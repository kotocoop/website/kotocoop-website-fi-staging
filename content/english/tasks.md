--- 
title: Tasks
date: 2023-01-03 22:00:00 +0000
aliases:
- /tasks
---

# Tasks

Here is a list of published tasks [https://kotocoop.org/task/](https://kotocoop.org/task/)



## Current tasks

### Technology

#### Starting the project: Koto design system and resources management software

[https://pad.kotocoop.org/p/r.59f85b7c4ac99e59a9b18d9d73da5bf8](https://pad.kotocoop.org/p/r.59f85b7c4ac99e59a9b18d9d73da5bf8)



#### Project: CNC machine

[https://pad.kotocoop.org/p/r.1ab4e5df0e9a839dc2821a9617d1b112](https://pad.kotocoop.org/p/r.1ab4e5df0e9a839dc2821a9617d1b112)



### Building

#### How to use locally cut timber

[https://pad.kotocoop.org/p/r.0c845f315539b5f942c6c8068f859903](https://pad.kotocoop.org/p/r.0c845f315539b5f942c6c8068f859903)



### Auroora

#### Gathering used stuff we need in Auroora

[https://pad.kotocoop.org/p/r.b14ede28b0183bbb3ef1a3ad3229147b](https://pad.kotocoop.org/p/r.b14ede28b0183bbb3ef1a3ad3229147b)

#### Outdoors plan

[https://pad.kotocoop.org/p/r.0a5d6f75de3e3284820024bc5b7a4a99](https://pad.kotocoop.org/p/r.0a5d6f75de3e3284820024bc5b7a4a99)

#### Designing a heating system with solar energy usage in Auroora

[https://pad.kotocoop.org/p/r.d239bca63300cdf85198a19f317be141](https://pad.kotocoop.org/p/r.d239bca63300cdf85198a19f317be141)



### Content



#### Koto and stages

[https://pad.kotocoop.org/p/r.8b60a496baf9a3d399c949191040d1ad](https://pad.kotocoop.org/p/r.8b60a496baf9a3d399c949191040d1ad)

