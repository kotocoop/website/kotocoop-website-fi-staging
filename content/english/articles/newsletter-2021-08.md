---

title: Newsletter September 2021

image: /img/articles/newsletter-september-2021.jpg

aliases:

- /newsletters/2021/september

date: 2021-09-09 21:00:00 +0000
---

Greetings from Koto Co-op!

This is our first newsletter and we have been an active co-op for over a year now. There is a lot to tell so I'll try not to go too much into details and first I want to take this opportunity to congratulate and thank everyone involved. Our (currently) small community is what is giving me hope for the future.

People often think that our purpose is to found an intentional community living somewhere. I would say that is one the ways our purpose is manifested and our purpose is larger than that. We are trying to develop social structures and open-source technologies that make a better world possible. Freeing people to do that development is one important way. Our purpose is explained in [our introductory video](https://odysee.com/@kotocoop:3/introduction:5).

So far we have organized into multiple teams which are communications, video creation, technology, development and coordination teams. They are all services for the community and do not work under administration as explained in [this video about structures of Koto Co-op](https://odysee.com/@kotocoop:3/Structures-of-Koto-Co-op:4).

If you want to join the effort, watch this ["How to join" video](https://odysee.com/@kotocoop:3/How-to-join-Koto-Co-op:0)

We decided that our main goal for this year is to found the first livable unit. It took some time to figure out the funding and find the right place, but I'm happy to say we have purchased Auroora. It is a property of an old school building and some land in the eastern part of Finland. The first resident will move in in September and we are looking for more people. If you are interested, please fill in this [questionnaire](https://kotocoop.org/joiningaunit). More information about the place is found on [this blog post](https://kotocoop.org/blog/2021-07-16-a-place-for-our-first-unit/)

There is a lot to do and we are always happy to welcome new people. Don't hesitate to join this important work! We are looking into how different units like Auroora should work and be designed. All the designs and technologies will be free for everybody to use, so it is valuable work even if you choose to start something similar to Koto.

See you later!

\- Juuso Vilmunen